// class Modal {

//     constructor() {
//         this.modalContainer = document.createElement('div');
//         this.modalContainer.className = 'modal';
//         document.body.appendChild(this.modalContainer);

//         const contentContainer = document.createElement('div');
//         contentContainer.className = 'container';
//         this.modalContainer.appendChild(contentContainer);

//         const closeButton = document.createElement('button');
//         closeButton.innerHTML = '&times;';
//         closeButton.className = 'close-button';
//         contentContainer.appendChild(closeButton);
//         closeButton.addEventListener('click', this.close.bind(this));

//         this.content = document.createElement('div');
//         contentContainer.appendChild(this.content);
//     }

//     set html(value) {
//         this.content.innerHTML = value;
//     }

//     open() {
//         this.modalContainer.classList.add('open');
//     }

//     close() {
//         this.modalContainer.classList.remove('open');
//     }

// }

// let i = 0;
// m = new Modal();

// document.getElementById('forgot-password-trigger').addEventListener('click', () => {
//     m.html = `You've opened this modal ${++i} times`;
//     m.open();
// })

class Modal {

    constructor() {
        this.modalContainer = document.createElement('div');
        this.modalContainer.className = 'flex-container-full';
        this.modalContainer.classList.add('modal');
        document.body.appendChild(this.modalContainer);

        const contentContainer = document.createElement('div');
        contentContainer.className = 'container';
        this.modalContainer.appendChild(contentContainer);

        const closeButton = document.createElement('button');
        closeButton.className = 'close-button';
        closeButton.innerHTML = '&times;';
        contentContainer.appendChild(closeButton);
        closeButton.addEventListener('click', this.close.bind(this));

        this.header = document.createElement('h2');
        contentContainer.appendChild(this.header);

        this.headerDescription = document.createElement('p');
        contentContainer.appendChild(this.headerDescription);

        const formContainer = document.createElement('form');
        contentContainer.appendChild(formContainer);

        const elementForm = document.createElement('div');
        elementForm.className = 'element-form';
        formContainer.appendChild(elementForm);

        this.emailInput = document.createElement('input');
        this.emailInput.setAttribute('type', 'email');
        this.emailInput.setAttribute('placeholder', 'Alamat Email');
        this.emailInput.setAttribute('required', 'true');
        elementForm.appendChild(this.emailInput);
        

        this.recoverButton = document.createElement('button');
        this.recoverButton.innerHTML = 'Kirim';
        this.recoverButton.className = 'btn';
        this.recoverButton.classList.add('shadow', 'btn-default');
        formContainer.appendChild(this.recoverButton);
    }

    set headerText(value) {
        this.header.innerHTML = value;
    }
    set headerTextDescription(value) {
        this.headerDescription.innerHTML = value;
    }
    open() {
        this.modalContainer.classList.add('open');
    }
    close() {
        this.modalContainer.classList.remove('open');
    }
    
}

let m = new Modal();

document.getElementById('forgot-password-trigger').addEventListener('click', () => {
    m.headerText = 'Lupa Password Kamu?';
    m.headerTextDescription = 'Kami siap membantu mengembalikan password kamu yang hilang atau lupa. Silahkan masukkan alamat email kamu di bawah ini.';
    m.open();
})